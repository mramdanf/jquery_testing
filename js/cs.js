$(document).ready(function() {

	var $listTabs  = $('ul#tabs li a');


	var tableFilter = {
		token: '45fb5eca8284a3316358daafc2768ad4',
		and_filter: {
			f_pesanan_statusorder: 'Terkonfirmasi'
		}
	};

	var orderTbl = $('table#pemesanan').DataTable({
		serverSide: true,
		processing: true,
		ordering: true,
		lengthChange: false,
		searching: false,
		ajax: {
	        url: 'http://localhost/cms/index.php/api/wh/view_all_orders',
	        type: 'GET',
	        data: tableFilter

	    },
	    "columns": [
            { "data": "f_pesanan_id" },
            { "data": "f_pesanan_date" },
            { "data": "f_pesanan_produkname" },
            { "data": "f_pesanan_qty" },
            { "data": "f_pesanan_totaltagihan" },
            { "data": "f_pesanan_namalengkap" },
            { "data": "f_pesanan_phone" },
            { "data": "f_pesanan_email" },
            { "data": "f_pesanan_statusorder" },
            { "data": "f_pesanan_statuskirim" }
        ]
	});

	$listTabs.click(function(ev) {
		ev.preventDefault();

		$listTabs.removeClass('active');
		$(this).addClass('active');

		var activeTabText = $(this).text();

		if (activeTabText == 'Terkonfirmasi')
			tableFilter['and_filter']['f_pesanan_statusorder'] = 'Terkonfirmasi';
		else if (activeTabText == 'Dikemas')
			tableFilter['and_filter']['f_pesanan_statuskirim'] = 'Dikirim';

		orderTbl.order([2, "asc"]).draw();

	});

	
	
});